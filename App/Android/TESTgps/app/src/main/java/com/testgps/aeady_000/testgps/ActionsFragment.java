package com.testgps.aeady_000.testgps;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ActionsFragment.OnActionsFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ActionsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ActionsFragment extends Fragment implements
        FragmentCallback,
        missionSubFragment.OnMissionSubFragmentInteractionListener{

    private static final String TAG = ActionsFragment.class.getSimpleName();
    private static final String ARG_SECTION_NUMBER = "section_number";

    private HashMap missions;
    final String url = "http://ugrad.bitdegree.ca/~bethanydunfield/Espionage/Prototype/php/getMissions.php";
    Map<String, String> params;

    private OnActionsFragmentInteractionListener mListener;
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                Boolean resultCode = bundle.getBoolean(LocationService.LOCATION);
                if (resultCode == true) {
                    updateUserLocation();
                } else {
                    //error
                }
            }
        }
    };

    public static ActionsFragment newInstance( int sectionNumber ) {
        ActionsFragment fragment = new ActionsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public ActionsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            params = new HashMap<String, String>();
        }
    }

    @Override
    public void onStart(){
        super.onStart();


    }

    @Override
    public void onResume(){
        super.onResume();
        getActivity().registerReceiver(receiver, new IntentFilter(LocationService.NOTIFICATION));
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(receiver);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View V = inflater.inflate(R.layout.fragment_actions, container, false);
        // Inflate the layout for this fragment

        return V;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnActionsFragmentInteractionListener) activity;

        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        //created when fragment inserted into activity
        //can now access elements in the activity view
        Map<String, String> mySession = MySingleton.getSession(getActivity().getApplicationContext());

        if (mySession.get("username") != null) {
            Log.d(TAG, mySession.toString());
            ServerRequest mFetchTask = new ServerRequest( getActivity().getApplicationContext(), url, mySession, this );
            mFetchTask.execute((Void) null);

        } else {
            //error
        }
    }

    @Override
    public void onTaskDone(JSONObject data){
        Log.d( TAG, "onTaskDone" + data.toString() );
        updateFragment(data);
    }

    private void updateFragment(JSONObject data){

        Log.d(TAG, "updateFragment" + data.toString());

        FragmentManager fm = getChildFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        String type;
        String snippet;
        String fragTag;
        Double lat;
        Double lng;

        actionSubFragment mActionSubFragment;
        //bug
        fragTag = "bug";
        mActionSubFragment = actionSubFragment.newInstance(fragTag);
        ft.add(R.id.actions_linear, mActionSubFragment, fragTag);
        //stakeout
        fragTag = "stakeout";
        mActionSubFragment = actionSubFragment.newInstance(fragTag);
        ft.add(R.id.actions_linear, mActionSubFragment, fragTag);
        //bug
        fragTag = "sweep";
        mActionSubFragment = actionSubFragment.newInstance(fragTag);
        ft.add(R.id.actions_linear, mActionSubFragment, fragTag);
        //bug
        fragTag = "neutralize";
        mActionSubFragment = actionSubFragment.newInstance(fragTag);
        ft.add(R.id.actions_linear, mActionSubFragment, fragTag);

        missionSubFragment mMissionSubFragment;
        try {
            JSONArray jsonArray = data.getJSONArray("missions");
            for (int i = 0; i < jsonArray.length(); i++) {
                Log.d(TAG, "mission " + i + " :" + jsonArray.getJSONObject(i).toString());
                type = jsonArray.getJSONObject(i).getString("type");
                snippet = jsonArray.getJSONObject(i).getString("description");
                fragTag = type + i;
                lat = jsonArray.getJSONObject(i).getDouble("locLat");
                lng = jsonArray.getJSONObject(i).getDouble("locLong");

                mMissionSubFragment = missionSubFragment.newInstance(type, snippet, lat, lng, fragTag);

                ft.add(R.id.actions_linear, mMissionSubFragment, fragTag);

                //missions.put( fragTag, jsonArray.getJSONObject(i));
                //missions.put("fragment" + i, mMissionSubFragment);
            }

            ft.commit();
            fm.executePendingTransactions();

            //Log.d(TAG, "PARSED MISSIONS: " + missions.toString());
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "ERROR", e.toString());
        }

    }

    //TODO: have the update user location show and hide missions based on proximity
    public void updateUserLocation(){
        //actions to perform when new location data is available
    }



    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

    public void onMissionSubFragmentInteraction (Uri uri){
        //things
    }

    public interface OnActionsFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onActionsFragmentInteraction(Uri uri);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onActionsFragmentInteraction(uri);
        }
    }
}
