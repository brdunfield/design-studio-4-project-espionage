package com.testgps.aeady_000.testgps;

import android.app.Activity;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FeedFragment.OnNotificationFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FeedFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FeedFragment extends Fragment implements FragmentCallback {
    //TODO: remove debug TAG
    private static final String TAG = FeedFragment.class.getSimpleName();

    private static final String ARG_SECTION_NUMBER = "section_number";
    final String url = "http://ugrad.bitdegree.ca/~bethanydunfield/Espionage/Prototype/php/getFeedInfo.php";

    private OnNotificationFragmentInteractionListener mListener;

    public FeedFragment() {
        // Required empty public constructor
    }

    public static FeedFragment newInstance(int sectionNumber) {
        FeedFragment fragment = new FeedFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //fragment is constructed, by not in the view yet
        //special onCreate commands here
    }

    @Override
    public void onStart() {
        super.onStart();
        //called every time fragment becomes visible on screen
        //special onStart commands here
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //called when fragment view created the first time
        View V = inflater.inflate(R.layout.fragment_feed, container, false);
        // Inflate the layout for this fragment
        //can now work with visual elements
        return V;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //created when fragment inserted into activity
        //can now access elements in the activity view

        Map<String, String> mySession = MySingleton.getSession(getActivity().getApplicationContext());

        if (mySession.get("username") != null) {
            //Log.d(TAG, mySession.toString());

            ServerRequest mFetchTask = new ServerRequest(getActivity().getApplicationContext(), url, mySession, this);
            mFetchTask.execute((Void) null);

        } else {
            //error
        }
    }

    @Override
    public void onTaskDone(JSONObject data) {
        //Log.d(TAG, "onTaskDone" + data.toString());
        updateFragment(data);
    }

    private void updateFragment(JSONObject data) {
        //This is where we make use of the JSONobject that the server returns
        LinearLayout m_feedLayout = (LinearLayout) getActivity().findViewById(R.id.feed_linear);
        LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        try {
            JSONArray jsonArray = data.getJSONArray("feedEntries");
            for (int i = 0; i < jsonArray.length(); i++) {
                TextView mTextMessage = new TextView(getActivity());
                mTextMessage.setLayoutParams(lparams);
                TextView mTextTimestamp = new TextView(getActivity());
                mTextTimestamp.setLayoutParams(lparams);
                String toAdd = new String();

                toAdd = jsonArray.getJSONObject(i).get("message").toString();
                mTextMessage.setText(toAdd);

                toAdd = jsonArray.getJSONObject(i).get("time").toString();
                mTextTimestamp.setText(toAdd);
                mTextTimestamp.setBackgroundColor(Color.parseColor("#ebebeb"));

                m_feedLayout.addView(mTextTimestamp);
                m_feedLayout.addView(mTextMessage);
            }


        } catch (JSONException e) {
            e.printStackTrace();
            //Log.d(TAG + "ERROR", e.toString());
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnNotificationFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //if anything needs to be turned of when done with fragment
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnNotificationFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onNotificationFragmentInteraction(Uri uri);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onNotificationFragmentInteraction(uri);
        }
    }
}
