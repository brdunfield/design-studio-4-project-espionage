package com.testgps.aeady_000.testgps;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Handler;
import android.support.v13.app.FragmentPagerAdapter;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.model.LatLng;


public class MainActivity extends Activity
        implements ActionBar.TabListener,
        AgentFragment.OnAgentFragmentInteractionListener,
        FeedFragment.OnNotificationFragmentInteractionListener,
        DossierFragment.OnDossierFragmentInteractionListener,
        ActionsFragment.OnActionsFragmentInteractionListener,
        CustomMapFragment.OnMapFragmentInteractionListener,
        missionSubFragment.OnMissionSubFragmentInteractionListener,
        actionSubFragment.OnActionSubFragmentInteractionListener {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v13.app.FragmentStatePagerAdapter}.
     */
    private static final String TAG = MainActivity.class.getSimpleName();
    SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //start location service
        Intent locationIntent = new Intent(this, LocationService.class);
        locationIntent.setAction("startListening");
        startService(locationIntent);

        setContentView(R.layout.activity_main);

        // Set up the action bar.
        final ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        // When swiping between different sections, select the corresponding
        // tab. We can also use ActionBar.Tab#select() to do this if we have
        // a reference to the Tab.
        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                actionBar.setSelectedNavigationItem(position);
            }
        });

        // For each of the sections in the app, add a tab to the action bar.
        for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
            // Create a tab with text corresponding to the page title defined by
            // the adapter. Also specify this Activity object, which implements
            // the TabListener interface, as the callback (listener) for when
            // this tab is selected.
            actionBar.addTab(
                    actionBar.newTab()
                            .setText(mSectionsPagerAdapter.getPageTitle(i))
                            .setTabListener(this));
        }

        Handler m = new Handler();
        Timer myTimer = new Timer();
        myTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                checkIn();
            }
        }, 0, 30000);
    }

    @Override
    public void onResume(){
        super.onResume();
        //TODO: should location service be restarted?
    }

    @Override
    public void onStop(){
        //TODO: should location service be stopped?
        super.onStop();
    }

    @Override
    public void onDestroy(){
        Intent locationIntent = new Intent(this, LocationService.class);
        locationIntent.setAction("stopListening");
        startService(locationIntent);
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public void goToBrowser (){
        String url = "http://ugrad.bitdegree.ca/~bethanydunfield/Espionage/reportProblem.html";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);

    }

    /******************
     *
     * Fragment interaction events, if needed
     */
    public void onAgentFragmentInteraction(Uri uri){
        //things
    }

    public void onNotificationFragmentInteraction(Uri uri){
        //things
    }

    public void onDossierFragmentInteraction(Uri uri){
        //things
    }

    public void onActionsFragmentInteraction(Uri uri){
        //things
    }

    public void onMapFragmentInteraction (Uri uri){
        //things
    }

    public void onMissionSubFragmentInteraction (Uri uri){
        //things
    }

    public void onActionSubFragmentInteraction (Uri uri){
        //things
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_reportBug) {
            goToBrowser();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        // When the given tab is selected, switch to the corresponding page in
        // the ViewPager.
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            //return PlaceholderFragment.newInstance(position + 1);
            switch (position) {
                case 0:
                    return FeedFragment.newInstance(position + 1);
                case 1:
                    return CustomMapFragment.newInstance(position + 1);
                case 2:
                    return ActionsFragment.newInstance(position + 1);
                case 3:
                    return DossierFragment.newInstance(position + 1);
                case 4:
                    return AgentFragment.newInstance(position + 1);
                default:
                    return PlaceholderFragment.newInstance(position + 1);
            }
        }

        @Override
        public int getCount() {
            // Show 5 total pages.
            return 5;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return getString(R.string.title_section1).toUpperCase(l);
                case 1:
                    return getString(R.string.title_section2).toUpperCase(l);
                case 2:
                    return getString(R.string.title_section3).toUpperCase(l);
                case 3:
                    return getString(R.string.title_section4).toUpperCase(l);
                case 4:
                    return getString(R.string.title_section5).toUpperCase(l);
            }
            return null;
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     * Use this to Test new tabs
     * Template for new fragments
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }
    }

    public void checkIn(){

        //Log.d(TAG, "CHECK IN");
        String url = "http://ugrad.bitdegree.ca/~bethanydunfield/Espionage/Prototype/php/userCheckin.php";
        Map<String, String> params = new HashMap<String, String>();

        Map<String, String> session = MySingleton.getSession(getApplicationContext());
        LatLng position = MySingleton.getCurrentLatLng(getApplicationContext());

        String username = session.get("username");
        String latitude = new String (String.valueOf(position.latitude));
        String longitude = new String (String.valueOf(position.longitude));

        params.put("dataType", "checkIn");
        params.put("username", username);
        params.put("Location_Lat", latitude);
        params.put("Location_Long", longitude);


        if ( position.latitude != 0 ){
            ServerRequest mFetchTask = new ServerRequest(getApplicationContext(), url, params, null );
            mFetchTask.execute((Void) null);
        }
    }
}
