package com.testgps.aeady_000.testgps;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AgentFragment.OnAgentFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AgentFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AgentFragment extends Fragment implements FragmentCallback {
    //TODO: remove debug TAG
    private static final String TAG = FeedFragment.class.getSimpleName();

    private static final String ARG_SECTION_NUMBER = "section_number";
    final String url = "http://ugrad.bitdegree.ca/~bethanydunfield/Espionage/Prototype/php/getProfileInfo.php";

    private OnAgentFragmentInteractionListener mListener;

    public static AgentFragment newInstance( int sectionNumber ) {
        AgentFragment fragment = new AgentFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public AgentFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View V = inflater.inflate(R.layout.fragment_agent, container, false);
        // Inflate the layout for this fragment
        return V;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        //created when fragment inserted into activity
        //can now access elements in the activity view

        Map<String, String> mySession = MySingleton.getSession(getActivity().getApplicationContext());

        if (mySession.get("username") != null) {
            //Log.d(TAG, mySession.toString());

            ServerRequest mFetchTask = new ServerRequest( getActivity().getApplicationContext(), url, mySession, this );
            mFetchTask.execute((Void) null);

        } else {
            //error here
        }
    }

    @Override
    public void onTaskDone(JSONObject data){
        //Log.d( TAG, "onTaskDone" + data.toString() );
        updateFragment(data);
    };

    private void updateFragment(JSONObject data){
        //This is where we make use of the JSONobject that the server returns
        //Log.d( TAG, "updateFragment" + data.toString() );
        if ( AgentFragment.this.isVisible() ) {
            try {
                TextView mText;
                String msg;
                //access the parts of profileData
                JSONObject object = data.getJSONObject("profileData");

                //first name
                mText = (TextView) getActivity().findViewById(R.id.agent_fn);
                msg = mText.getText() + " " + object.getString("firstname");
                mText.setText(msg);
                //last name
                mText = (TextView) getActivity().findViewById(R.id.agent_ln);
                msg = mText.getText() + " " + object.getString("lastname");
                mText.setText(msg);
                //age
                mText = (TextView) getActivity().findViewById(R.id.agent_age);
                msg = mText.getText() + " " + object.getString("age");
                mText.setText(msg);
                //gender
                mText = (TextView) getActivity().findViewById(R.id.agent_gender);
                msg = mText.getText() + " " + object.getString("gender");
                mText.setText(msg);
                //haircolor
                mText = (TextView) getActivity().findViewById(R.id.agent_hair);
                msg = mText.getText() + " " + object.getString("haircolour");
                mText.setText(msg);
                //height
                mText = (TextView) getActivity().findViewById(R.id.agent_height);
                msg = mText.getText() + " " + object.getString("height");
                mText.setText(msg);
                //build
                mText = (TextView) getActivity().findViewById(R.id.agent_build);
                msg = mText.getText() + " " + object.getString("build");
                mText.setText(msg);
                //occupation
                mText = (TextView) getActivity().findViewById(R.id.agent_occupation);
                msg = mText.getText() + " " + object.getString("occupation");
                mText.setText(msg);
                //status

                String status = object.getString("alive");
                String exposed = object.getString("exposed");
                mText = (TextView) getActivity().findViewById(R.id.agent_status);

                if (status.equals("1") && exposed.equals("0")){ msg = mText.getText() + " ACTIVE"; }
                else if (status.equals("1") && exposed.equals("1")){ msg = mText.getText() + " EXPOSED"; }
                else if (status.equals("0")){ msg = mText.getText() + " NEUTRALIZED"; }

                mText.setText(msg);

            } catch (JSONException e) {
                e.printStackTrace();
                //Log.d(TAG + "ERROR", e.toString());
            }
        } else {
            //Log.d (TAG, "didn't update text because view not visible");
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnAgentFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnAgentFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onAgentFragmentInteraction(Uri uri);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onAgentFragmentInteraction(uri);
        }
    }
}
