package com.testgps.aeady_000.testgps;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.os.IBinder;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CustomMapFragment.OnMapFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CustomMapFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CustomMapFragment extends Fragment implements FragmentCallback{
    //TODO: remove debug TAG
    private static final String TAG = CustomMapFragment.class.getSimpleName();
    private static View view;
    private boolean firstUpdateUserLocation = false;
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                Boolean resultCode = bundle.getBoolean(LocationService.LOCATION);
                if (resultCode == true) {
                    updateUserLocation();
                } else {
                    //error
                }
            }
        }
    };

    private HashMap<String, JSONObject> missions;
    private MySingleton session;

    private static final String ARG_SECTION_NUMBER = "section_number";
    final String url = "http://ugrad.bitdegree.ca/~bethanydunfield/Espionage/Prototype/php/getMissions.php";

    private OnMapFragmentInteractionListener mListener;
    public GoogleMap googleMap = null;

    public static CustomMapFragment newInstance(int sectionNumber) {
        CustomMapFragment fragment = new CustomMapFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public CustomMapFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            missions = new HashMap<String, JSONObject>();
            session = MySingleton.getInstance(getActivity().getApplicationContext());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.fragment_map, container, false);
        } catch (InflateException e) {
        /* map is already there, just return view as it is */
        }
        return view;
    }

    @Override
    public void onResume(){
        super.onResume();
        getActivity().registerReceiver(receiver, new IntentFilter(LocationService.NOTIFICATION));
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(receiver);
    }

    private void setUpMapIfNeeded(){
        if(googleMap == null)
        {
            googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();

            if(googleMap != null)
            {
                googleMap.setMyLocationEnabled(true);
                LatLng startCoord = MySingleton.getCurrentLatLng(getActivity().getApplicationContext());
                //Log.d (TAG, "START COORDS FOR MAP: " + startCoord.toString());
                GoogleMapOptions options = new GoogleMapOptions();
                options.mapType(GoogleMap.MAP_TYPE_NORMAL)
                        .rotateGesturesEnabled(true)
                        .camera(new CameraPosition(startCoord, 13f, 0f, 0f));

            }
            else
            {
                //Log.d(TAG, "googleMap is null");
            }
        }
    }

    public void updateUserLocation(){
        if(googleMap != null)
        {
            LatLng newCoord = MySingleton.getCurrentLatLng(getActivity().getApplicationContext());
            CameraUpdate center = CameraUpdateFactory.newLatLngZoom( newCoord, 15 );
            if (firstUpdateUserLocation == false ) {
                googleMap.moveCamera(center);
                firstUpdateUserLocation = true;
            }
        }
        else
        {
            //Log.d(TAG, "googleMap is null");
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        //created when fragment inserted into activity
        //can now access elements in the activity view

        Map<String, String> mySession = MySingleton.getSession(getActivity().getApplicationContext());

        if (mySession.get("username") != null) {
            //Log.d( TAG, mySession.toString() );
            setUpMapIfNeeded();
            ServerRequest mFetchTask = new ServerRequest( getActivity().getApplicationContext(), url, mySession, this );
            mFetchTask.execute((Void) null);

        } else {
            //error
        }
    }

    @Override
    public void onTaskDone(JSONObject data){
        //Log.d( TAG, "onTaskDone" + data.toString() );
        updateFragment(data);
    };

    private void updateFragment(JSONObject data){
        if(googleMap != null) {
            //Log.d(TAG, "updateFragment" + data.toString());

            try {
                JSONArray jsonArray = data.getJSONArray("missions");
                for (int i = 0; i < jsonArray.length(); i++) {
                    missions.put("mission" + i, jsonArray.getJSONObject(i));
                    googleMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(jsonArray.getJSONObject(i).getDouble("locLat"), jsonArray.getJSONObject(i).getDouble("locLong")))
                                    .title(jsonArray.getJSONObject(i).getString("type"))
                                    .snippet(jsonArray.getJSONObject(i).getString("description"))
                    );
                }

                //Log.d(TAG, "PARSED MISSIONS: " + missions.toString());
            } catch (JSONException e) {
                e.printStackTrace();
                //Log.d(TAG + "ERROR", e.toString());
            }
        }
        else
        {
            //Log.d(TAG, "googleMap is null");
            setUpMapIfNeeded();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnMapFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }



    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnMapFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onMapFragmentInteraction(Uri uri);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onMapFragmentInteraction(uri);
        }
    }
}
