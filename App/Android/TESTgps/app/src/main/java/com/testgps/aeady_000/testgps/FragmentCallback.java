package com.testgps.aeady_000.testgps;

import org.json.JSONObject;

/**
 * Created by aeady_000 on 2014-12-07.
 */
public interface FragmentCallback {
    public void onTaskDone(JSONObject data);
}