package com.testgps.aeady_000.testgps;

/**
 * Created by aeady_000 on 2014-11-26.
 */
//TODO: remove the default login and pw from the session
import android.content.Context;
import android.graphics.Bitmap;
import android.location.Location;
import android.support.v4.util.LruCache;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.model.LatLng;

import java.util.HashMap;
import java.util.Map;

public class MySingleton {
    private static MySingleton mInstance;
    private static Map<String, String> mSession;
    private static Map<String, Location> mLocation;

    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    private static Context mCtx;

    //init the singleton and its internal request queue
    private MySingleton(Context context) {
        mCtx = context;
        mRequestQueue = getRequestQueue();

        mImageLoader = new ImageLoader(mRequestQueue,
                new ImageLoader.ImageCache() {
                    private final LruCache<String, Bitmap>
                            cache = new LruCache<String, Bitmap>(20);

                    @Override
                    public Bitmap getBitmap(String url) {
                        return cache.get(url);
                    }

                    @Override
                    public void putBitmap(String url, Bitmap bitmap) {
                        cache.put(url, bitmap);
                    }
                });
    }

    //returns the singleton so that the requestQueue can be used in any activity
    //instantiates if needed
    public static synchronized MySingleton getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new MySingleton(context);
        }
        return mInstance;
    }

    //This initiates the session on login
    public static void initSession(String username, String token){
        //TODO: fix this check to not use username or token, but throw error
        if ( mSession == null || username == null || token == null ) {
            mSession = new HashMap<String, String>();
            mSession.put("username", null);
            mSession.put("token", null);
        }
        mSession.put("username", username);
        mSession.put("token", token);
    }

    //This returns the session to any requesting activity, needed to query database
    public static synchronized Map<String, String> getSession(Context context) {
        if ( mSession == null ) {
            mSession = new HashMap<String, String>();
            mSession.put("username", "test@email.com");
            mSession.put("token", "password");
        }
        return mSession;
    }

    public static synchronized  void updateLocation(Location loc){
        if ( mLocation == null ){
            mLocation = new HashMap<String, Location>();
        }
        mLocation.put("location", loc);
    }

    public static synchronized Location getCurrentLocation(Context context){
        if (mLocation == null) {
            mLocation = new HashMap<String, Location>();
            mLocation.put("location", new Location("default"));
        }
        return mLocation.get("location");
    }

    public static synchronized LatLng getCurrentLatLng(Context context){
        if (mLocation == null) {
            mLocation = new HashMap<String, Location>();
            mLocation.put("location", new Location("default"));
        }
        LatLng latLng = new LatLng( mLocation.get("location").getLatitude(), mLocation.get("location").getLongitude() );
        return latLng;
    }

    //This returns the requestQueue for use
    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        }
        return mRequestQueue;
    }

    //Adds an item to the requestQueue
    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }

    //Returns the imageLoader for use
    public ImageLoader getImageLoader() {
        return mImageLoader;
    }
}
