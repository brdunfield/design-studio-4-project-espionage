package com.testgps.aeady_000.testgps;

import android.app.Dialog;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

public class LocationService extends Service implements
        LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener  {
    private MySingleton session;
    private final IBinder mBinder = new MyBinder();
    private final static int
            UPDATE_INTERVAL = 5000;
    private static final int
            FASTEST_INTERVAL = 1000;
    public static String LOCATION = "location";
    public static final String NOTIFICATION = "com.testgps.aeady_000.testgps";

    private GoogleApiClient mLocationClient;
    private LocationRequest mLocationRequest;

    @Override
    public int onStartCommand(final Intent intent, final int flags, final int startId) {
        if (session == null) {
            session = MySingleton.getInstance(getApplicationContext());
        }
        if (mLocationClient == null){
            mLocationClient = new GoogleApiClient.Builder(getApplicationContext())
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();

            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(UPDATE_INTERVAL);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        }

        //Log.d("LOCATIONSRVC", "intialized: " + intent.getAction());

        if (intent.getAction().equals("startListening")) {
            //Log.d("LOCATIONSRVC", "start listening");
            mLocationClient.connect();

        }
        else if (intent.getAction().equals("stopListening")) {
            //Log.d("LOCATIONSRVC", "stop listening");
            LocationServices.FusedLocationApi.removeLocationUpdates(mLocationClient, this);
            mLocationClient.disconnect();
            stopSelf();
        }

        return START_STICKY;

    }

    @Override
    public void onLocationChanged(Location location) {
        LatLng loc = new LatLng( location.getLatitude(), location.getLongitude() );
        //Log.d("LOCATIONSRVC", "locationChange" + loc.toString());
        session.updateLocation(location);

        Intent intent = new Intent(NOTIFICATION);
        intent.putExtra(LOCATION, true);
        sendBroadcast(intent);
    }

    @Override
    public void onConnected(Bundle dataBundle) {
        if (mLocationClient.isConnected()) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mLocationClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public IBinder onBind(final Intent intent) {
        return mBinder;
    }

    public class MyBinder extends Binder {
        LocationService getService() {
            return LocationService.this;
        }
    }
}