package com.testgps.aeady_000.testgps;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import java.util.HashMap;
import java.util.Map;


public class defaultActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_default);

        MySingleton.initSession(null, null);
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);

        checkSession();
    }

    @Override
    protected void onStart(){
        super.onStart();
     }

    @Override
    protected void onResume(){
        super.onResume();
        checkSession();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_default, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void checkDistance(View view) {
        Intent intent = new Intent(this, distanceActivity.class);
        startActivity(intent);
    }

    public void login(View view) {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    public void checkSession(){

        Button mLoginButton = (Button) findViewById(R.id.login_btn);
        Button mDistanceButton = (Button) findViewById(R.id.distance_btn);

        Map <String, String> session = new HashMap<String, String>(MySingleton.getSession(this.getApplicationContext()));

        if (session.get("username") != null && session.get("token") != null){
            mDistanceButton.setVisibility(View.VISIBLE);
            mLoginButton.setVisibility(View.INVISIBLE);
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        } else {
            //Intent intent = new Intent(this, LoginActivity.class);
            //startActivity(intent);
            mDistanceButton.setVisibility(View.INVISIBLE);
            mLoginButton.setVisibility(View.VISIBLE);
        }

    }


}
