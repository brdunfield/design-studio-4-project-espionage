package com.testgps.aeady_000.testgps;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import javax.security.auth.callback.Callback;

/**
 * Created by aeady_000 on 2014-11-28.
 */

public class ServerRequest extends AsyncTask<Void, Void, Void> {
    private FragmentCallback mFragmentCallback;
    private RequestQueue queue;
    private Map<String, String> parameters;
    private JSONObject data;
    private String urlJson;

    ServerRequest(){}

    ServerRequest( Context context, String url, Map<String, String> params, FragmentCallback mFragCallback) {
        parameters = params;
        urlJson = url;
        queue = MySingleton.getInstance(context).getRequestQueue();
        mFragmentCallback = mFragCallback;
        //Log.d("SERVERREQUEST", "called");
    }

    @Override
    protected Void doInBackground(Void... params) {
        //Log.d("SERVERREQUEST", "asynch: " + parameters.get("dataType"));
        if ( parameters.get("dataType") != null ){
            StringRequest postRequest = new StringRequest(Request.Method.POST, urlJson,
                    new Response.Listener<String>()
                    {
                        @Override
                        public void onResponse(String response) {
                            if (mFragmentCallback != null) {
                                //Log.d("SERVERREQUEST", "success!");
                                try {
                                    JSONObject responseObj = new JSONObject(response);
                                    if (parameters.get("dataType") != "checkIn") {//responseObj.put("response", response);
                                        mFragmentCallback.onTaskDone(responseObj);
                                    }
                                } catch (JSONException e) {
                                    //Log.d ("SERVERREQUEST", "error on response: " + e.getMessage());
                                }

                            }
                        }
                    },
                    new Response.ErrorListener()
                    {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            //Log.d("Error.Response", error.networkResponse.toString());
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams()
                {
                    Map<String, String>  params = new HashMap<String, String>();
                    params = parameters;
                    return params;
                }
            };

            //Log.d("SERVERREQUEST", "asynch String called");
            queue.add(postRequest);
        } else {
            CustomJsonRequest req = new CustomJsonRequest(Request.Method.POST, urlJson, parameters,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            if (mFragmentCallback != null) {
                                //Log.d("SERVERREQUEST", "success!");
                                mFragmentCallback.onTaskDone(response);
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                   // Log.d("SERVERREQUEST", "error: " + error.getMessage());
                }
            });

            //Log.d("SERVERREQUEST", "asynch JSON called");
            queue.add(req);
        }
        return null;
    }

    /*@Override
    protected void onPostExecute() {

    }
    */
    @Override
    protected void onCancelled() {

    }
}





