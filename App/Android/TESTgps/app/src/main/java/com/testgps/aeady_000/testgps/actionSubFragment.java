package com.testgps.aeady_000.testgps;

import android.app.Activity;
import android.app.FragmentManager;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link actionSubFragment.OnActionSubFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link missionSubFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class actionSubFragment extends Fragment implements FragmentCallback {

    private static final String TAG = ActionsFragment.class.getSimpleName();
    private static final String ARG_TITLE = "title";
    private static final String ARG_SNIPPET = "snippet";
    private static final String ARG_LAT = "lat";
    private static final String ARG_LONG = "long";
    private static final String ARG_TAG = "tag";
    private OnActionSubFragmentInteractionListener mListener;

    private String type;
    private String description;
    private Double latitude;
    private Double longitude;
    private String tag;

    public static actionSubFragment newInstance(String title) {
        actionSubFragment fragment = new actionSubFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TITLE, title);
        args.putString(ARG_TAG, title);
        Log.d(TAG, "actionSUB: " + args.toString());
        fragment.setArguments(args);
        return fragment;
    }

    public actionSubFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            type = getArguments().getString(ARG_TITLE);
            description = getArguments().getString(ARG_SNIPPET);
            latitude = getArguments().getDouble(ARG_LAT);
            longitude = getArguments().getDouble(ARG_LONG);
            tag = getArguments().getString(ARG_TAG);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View V = inflater.inflate(R.layout.fragment_action_sub, container, false);
        // Inflate the layout for this fragment

        return V;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        Fragment fragment = getFragmentManager().findFragmentByTag(tag);
        View viewf = fragment.getView();


        Button button = (Button) viewf.findViewById(R.id.actionBtn);
        if (button != null) {
            button.setTag(type);
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    action(v);
                }
            });
            button.setText(type);
            button.setAllCaps(true);
        }
    }

    public void action(View view) {
        Map<String, String> params = new HashMap<String, String>();

        Map<String, String> session = MySingleton.getSession(getActivity().getApplicationContext());
        LatLng position = MySingleton.getCurrentLatLng(getActivity().getApplicationContext());

        String username = session.get("username");
        String latitude = new String (String.valueOf(position.latitude));
        String longitude = new String (String.valueOf(position.longitude));

        params.put("dataType", "String");
        params.put("username", username);
        params.put("Location_Lat", latitude);
        params.put("Location_Long", longitude);

        String url = new String("http://ugrad.bitdegree.ca/~bethanydunfield/Espionage/Prototype/php/");

        switch (view.getTag().toString()) {
            case ("bug"):
                Toast.makeText(getActivity(), "bug", Toast.LENGTH_SHORT).show();
                params.put("actionType", "bug");
                url += "performAgentAction.php";
                break;
            case ("stakeout"):
                Toast.makeText(getActivity(), "stake out", Toast.LENGTH_SHORT).show();
                params.put("actionType", "stakeout");
                url += "performAgentAction.php";
                break;
            case ("sweep"):
                Toast.makeText(getActivity(), "sweep", Toast.LENGTH_SHORT).show();
                params.put("actionType", "sweep");
                url += "performAgentAction.php";
                break;
            case ("neutralize"):
                Toast.makeText(getActivity(), "neutralize", Toast.LENGTH_SHORT).show();
                params.put("actionType", "neutralize");
                url += "performNeutralization.php";
                break;
            case ("hack"):
                Toast.makeText(getActivity(), "hack", Toast.LENGTH_SHORT).show();
                params.put("actionType", "hacking");
                url += "performMissionAction.php";
                break;
            case ("safehouse"):
                Toast.makeText(getActivity(), "safehouse", Toast.LENGTH_SHORT).show();
                params.put("actionType", "safehouse");
                url += "performMissionAction.php";
                break;
            case ("rendezvous"):
                Toast.makeText(getActivity(), "rendezvous", Toast.LENGTH_SHORT).show();
                params.put("actionType", "rendezvous");
                url += "performMissionAction.php";
                break;
            case ("supplydrop"):
                Toast.makeText(getActivity(), "supply drop", Toast.LENGTH_SHORT).show();
                params.put("actionType", "supplydrop");
                url += "performMissionAction.php";
                break;
            case ("deaddrop"):
                Toast.makeText(getActivity(), "dead drop", Toast.LENGTH_SHORT).show();
                params.put("actionType", "deaddrop");
                url += "performMissionAction.php";
                break;
            case ("deaddroppickup"):
                Toast.makeText(getActivity(), "pickup dead drop", Toast.LENGTH_SHORT).show();
                params.put("actionType", "deaddroppickup");
                url += "performMissionAction.php";
                break;
        }

        if (session.get("username") != null) {
            Log.d(TAG, session.toString());

            ServerRequest mFetchTask = new ServerRequest( getActivity().getApplicationContext(), url, params, this );
            mFetchTask.execute((Void) null);

        } else {
            //error
        }
    }

    @Override
    public void onTaskDone(JSONObject data){
        Log.d( TAG, "onTaskDone" + data.toString() );
        //updateFragment(data);
    };

    private void updateFragment(JSONObject data){
        //This is where we make use of the JSONobject that the server returns

        Log.d( TAG, "updateFragment" + data.toString() );

    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onActionSubFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnActionSubFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnActionSubFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onActionSubFragmentInteraction(Uri uri);
    }

}
