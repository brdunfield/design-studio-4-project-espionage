package com.testgps.aeady_000.testgps;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DossierFragment.OnDossierFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DossierFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DossierFragment extends Fragment implements FragmentCallback {
    //TODO: remove debug TAG
    private static final String TAG = FeedFragment.class.getSimpleName();

    private static final String ARG_SECTION_NUMBER = "section_number";
    final String url = "http://ugrad.bitdegree.ca/~bethanydunfield/Espionage/Prototype/php/getDossierInfo.php";

    private OnDossierFragmentInteractionListener mListener;

    public static DossierFragment newInstance(int sectionNumber) {
        DossierFragment fragment = new DossierFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public DossierFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View V = inflater.inflate(R.layout.fragment_dossier, container, false);
        // Inflate the layout for this fragment
        return V;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //created when fragment inserted into activity
        //can now access elements in the activity view

        Map<String, String> mySession = MySingleton.getSession(getActivity().getApplicationContext());
        TextView mText = (TextView) getActivity().findViewById(R.id.textDossier);

        if (mySession.get("username") != null) {
            //Log.d(TAG, mySession.toString());

            ServerRequest mFetchTask = new ServerRequest(getActivity().getApplicationContext(), url, mySession, this);
            mFetchTask.execute((Void) null);

        } else {
            mText.setText("error");
        }
    }

    @Override
    public void onTaskDone(JSONObject data) {
        //Log.d(TAG, "onTaskDone" + data.toString());
        updateFragment(data);
    }

    ;

    private void updateFragment(JSONObject data) {
        //This is where we make use of the JSONobject that the server returns
        TextView mText = (TextView) getActivity().findViewById(R.id.textDossier);
        String toAdd = new String();
        try {
            JSONArray jsonArray = data.getJSONArray("dossierEntries");
            for (int i = 0; i < jsonArray.length(); i++) {
                toAdd = mText.getText().toString();
                toAdd += "\nAgent #: " + jsonArray.getJSONObject(i).get("targetID").toString();
                toAdd += "\t\t" + jsonArray.getJSONObject(i).get("target_LName").toString();
                toAdd += ", \t" + jsonArray.getJSONObject(i).get("target_FName").toString();
                toAdd += "\n\t\tStatus: " + jsonArray.getJSONObject(i).get("target_Alive").toString();

                mText.setText(toAdd);
            }


        } catch (JSONException e) {
            e.printStackTrace();
            //Log.d(TAG + "ERROR", e.toString());
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnDossierFragmentInteractionListener) activity;

        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnDossierFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onDossierFragmentInteraction(Uri uri);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onDossierFragmentInteraction(uri);
        }
    }

}
