<?php
    header('Access-Control-Allow-Origin: *');
    include('connect_DB.php');
    
    // Get username from post???
    $username = trim($_POST['username']);

    // Get Agent ID from DB
    $sql = "SELECT Agent_ID FROM Espionage_Users WHERE Email='$username'";
    $queryResult = mysql_query($sql);
    if ($queryResult) {
        if(mysql_num_rows($queryResult) > 0) {
            while($rowData = mysql_fetch_assoc($queryResult)) {
                $id = $rowData['Agent_ID'];
            }
        } else {
            $arr = array('success' => false, 'error' => "Error getting Agent ID from Database");
            header('Content-type: application/json');
            echo json_encode($arr);
            exit;
        }
    }

    // Get info known by this agent
    $sql = "SELECT * FROM Espionage_Dossier WHERE Agent_ID='$id'";
    $queryResult = mysql_query($sql);
    if ($queryResult) {
        if(mysql_num_rows($queryResult) > 0) {
            $dossierEntries = array();
            while($rowData = mysql_fetch_assoc($queryResult)) {
                // get target gent's alive / dead status
                $targetID = $rowData['Target_ID'];
                $alive = "SELECT `Alive` FROM `Espionage_Users` WHERE Agent_Id='$targetID'";
                $aliveRes = mysql_query($alive);
                if ($aliveRes) {
                    if (mysql_numrows($aliveRes) > 0) {
                        while($aliveData = mysql_fetch_assoc($aliveRes)) {
                            $dossierEntry = array(
                                'targetID' => $targetID,
                                'target_FName' => $rowData['Target_FName'],
                                'target_LName' => $rowData['Target_LName'],
                                'target_Age' => $rowData['Target_Age'],
                                'target_Gender' => $rowData['Target_Gender'],
                                'target_Alive' => $aliveData['Alive'],
                                'target_HairColour' => $rowData['Target_HairColour'],
                                'target_Height' => $rowData['Target_Height'],
                                'target_Build' => $rowData['Target_Build'],
                                'target_Occupation' => $rowData['Target_Occupation']
                            );
                        }
                    } else {
                        $arr = array('success' => false, 'error' => "The target user does not exist?? Should not happen");
                        header('Content-type: application/json');
                        echo json_encode($arr);
                        exit;
                    }
                } else {
                    $arr = array('success' => false, 'error' => "could not get Alive / Dead status of agent");
                    header('Content-type: application/json');
                    echo json_encode($arr);
                    exit;
                }
                array_push($dossierEntries, $dossierEntry);
            }
            // output to client
            $arr = array('dossierEntries' => $dossierEntries);
            header('Content-type: application/json');
            echo json_encode($arr);
            exit;
        } else {
            $arr = array('success' => false, 'error' => "This Agent has No Dossier Data");
            header('Content-type: application/json');
            echo json_encode($arr);
            exit;
        }
    } else {
        $arr = array('success' => false, 'error' => "Error selecting Dossier entries from Database");
        header('Content-type: application/json');
        echo json_encode($arr);
        exit;
    }

?>
