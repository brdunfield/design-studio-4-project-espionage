SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


CREATE TABLE IF NOT EXISTS `Espionage_Checkins` (
  `key` int(11) NOT NULL,
  `Agent_ID` int(11) NOT NULL,
  `Location_Lat` double NOT NULL,
  `Location_Long` double NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `Espionage_MissionCompletion` (
  `key` int(11) NOT NULL,
  `Agent_ID` int(11) NOT NULL,
  `Mission_ID` int(11) NOT NULL,
  `Time_Completed` datetime NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `Espionage_Missions` (
  `Mission_ID` int(11) NOT NULL,
  `Description` text NOT NULL,
  `Type` varchar(100) NOT NULL,
  `Location_Lat` double NOT NULL,
  `Location_Long` double NOT NULL,
  PRIMARY KEY (`Mission_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `Espionage_Users` (
  `Agent_ID` int(11) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Password` tinytext NOT NULL,
  `First_Name` tinytext NOT NULL,
  `Last_Name` tinytext NOT NULL,
  `Gender` char(1) NOT NULL,
  `Age` int(11) NOT NULL,
  `Location_Lat` double NOT NULL,
  `Location_Long` double NOT NULL,
  `Location_Broadcast` tinyint(1) NOT NULL,
  `Alive` tinyint(1) NOT NULL,
  PRIMARY KEY (`Agent_ID`),
  UNIQUE KEY `Email` (`Email`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
