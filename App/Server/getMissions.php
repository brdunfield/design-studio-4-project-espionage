<?php
    header('Access-Control-Allow-Origin: *');
    include('connect_DB.php');

    // Get Relevant data from POST
    $username = trim($_POST['username']);    

    // Get Agent ID from DB
    $sql = "SELECT `Agent_ID`, `Location_Broadcast` FROM Espionage_Users WHERE Email='$username'";
    $queryResult = mysql_query($sql);
    if ($queryResult) {
        if(mysql_num_rows($queryResult) > 0) {
            while($rowData = mysql_fetch_assoc($queryResult)) {
                $id = $rowData['Agent_ID'];
                $exposed = $rowData['Location_Broadcast'];
            }
        } else {
            //die("Could not get Agent Id from DB");
            $arr = array('success' => false, 'error' => "Error getting Agent ID from Database");
            header('Content-type: application/json');
            echo json_encode($arr);
        }
    }    
    // Lookup any currently active missions in DB (That user has not already completed
    // TODO: Verify date with mission start / end dates
    // TODO: Compare mission list to completed missions by this agent - don't return completed missions!!

    $date = date("Y-m-d H:i:s");
    $sql = "SELECT * FROM Espionage_Missions WHERE `Mission_ID` NOT IN (
                SELECT `Mission_ID` FROM Espionage_MissionCompletion WHERE Agent_ID='$id')
                AND '$date' BETWEEN `StartDate` AND `EndDate`";
    $queryResult = mysql_query($sql);

    if ($queryResult) {
        if(mysql_num_rows($queryResult) > 0) {
            $missions = array();
            while($rowData = mysql_fetch_assoc($queryResult)) {
                $mission = array(
                    'type' => $rowData['Type'],
                    'description' => $rowData['Description'],
                    'locLat' => $rowData['Location_Lat'],
                    'locLong' => $rowData['Location_Long']);
                array_push($missions, $mission);
            }
            // output to client
            $arr = array('success' => true, 'missions' => $missions);
            header('Content-type: application/json');
            echo json_encode($arr);
        } else {
            $arr = array('success' => false, 'error' => "No Missions found");
            header('Content-type: application/json');
            echo json_encode($arr);
        }
    }

?>
