<?php
    header('Access-Control-Allow-Origin: *');
    include('connect_DB.php');

    $sql = "SELECT `Location_Lat`, `Location_Long` FROM Espionage_Users WHERE Location_Broadcast=1 AND ALIVE=1";
    $queryResult = mysql_query($sql);

    if ($queryResult) {
        if(mysql_num_rows($queryResult) > 0) {
            $agents = array();
            while($rowData = mysql_fetch_assoc($queryResult)) {
                $agent = array(
                    'locLat' => $rowData['Location_Lat'],
                    'locLong' => $rowData['Location_Long']);
                array_push($agents, $agent);
            }
            // output to client
            $arr = array('success' => true, 'broadcastingAgents' => $agents);
            header('Content-type: application/json');
            echo json_encode($arr);
        } else {
            $arr = array('success' => true, 'error' => "No Player Broadcasting");
            header('Content-type: application/json');
            echo json_encode($arr);
        }
    }

?>
