function initialize() {
    // Init Google Map centered on Ottawa
    var mapOptions = {
      center: { lat: 45.4, lng: -75.7},
      zoom: 13
    };
    var map = new google.maps.Map(document.getElementById('tab_map'),
        mapOptions);
    
    
    // Plot positions of available missions
    
    $.post ( "http://ugrad.bitdegree.ca/~bethanydunfield/Espionage/Prototype/php/getMissions.php", {username: localStorage['username']})
        .done (
            function(data) {
                if (data.error) {
                    console.log(data.error);
                } else {
                    var markers = [];
                    var infoWindows = [];
                    for (var i=0; i < data.missions.length; i++) {
                        var m = data.missions[i];
                        infoWindows[i] = new google.maps.InfoWindow({
                            content: m.description
                        });
                        
                        var latLong = new google.maps.LatLng(m.locLat, m.locLong);
                        
                        markers[i] = new google.maps.Marker( {
                            position: latLong,
                            map: map,
                            title: "Mission!"
                        });
                        // Add event listener to pull up mission description on user click
                        google.maps.event.addListener(markers[i], 'click', function(index) {
                            // This interior function is for closure - to ensure we get the correct marker / infowindow
                            return function() {
                                for (var j=0; j < infoWindows.length; j++) {
                                    infoWindows[j].close();
                                }
                                infoWindows[index].open(map, markers[index]);
                            }
                        }(i));
                    }
                }
            }
        );
    
    
    // Plot any broadcasting agents on the map
    $.post ( "http://ugrad.bitdegree.ca/~bethanydunfield/Espionage/Prototype/php/getBroadcastingAgents.php", {})
        .done (
            function(data) {
                if (data.error) {
                    console.log(data.error);
                } else {
                    for (var i=0; i < data.broadcastingAgents.length; i++) {
                        var a = data.broadcastingAgents[i];
                        var latLong = new google.maps.LatLng(a.locLat, a.locLong);
                        
                        var marker = new google.maps.Marker( {
                            position: latLong,
                            map: map,
                            title: "Enemy Agent!"
                        });
                    }
                }
            }
        );

    // Plot position of device on the map
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var latLong = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            /*
            var infowindow = new google.maps.InfoWindow({
                map: map,
                position: latLong,
                content: 'Location found using HTML5.'
            });
            */
            var marker = new google.maps.Marker({
                position: latLong,
                map: map,
                title:"Your Device"
            });
            
        }, function(error) {
            console.log("Error - unable to find location - check browser security settings");
        }, {timeout: 5000}
        );
    } else {
        console.log("not supported");// Not supported
    }
}


$(document).ready(function() {
    // Call initialize function
    google.maps.event.addDomListener(window, 'load', initialize);    

});