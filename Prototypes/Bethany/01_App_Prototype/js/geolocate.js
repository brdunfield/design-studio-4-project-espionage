$(document).ready(function() {
    // This function handles continually polling the user's location and updating the database
    var pollInterval;
    if (navigator.geolocation) {
        // initial update - then update every minute the page is loaded
        updatePosition();
        pollInterval = setInterval(function() { updatePosition() }, 60000);
    } else {
        console.log("not supported");// Not supported
    }

});

function updatePosition() {
    console.log("Updating Position");
    navigator.geolocation.getCurrentPosition(function(position) {
        //position.coords.latitude, position.coords.longitude
        
        // Send updated position to DB
        var sendData = {
            "Location_Lat": position.coords.latitude,
            "Location_Long": position.coords.longitude,
            "username": localStorage["username"],
        };
        $.post ( "http://ugrad.bitdegree.ca/~bethanydunfield/Espionage/Prototype/php/userCheckin.php", sendData)
            .done (function(data) {
                console.log(data);
                if (data.error) {
                    showErrorDiv(data, "Automatic Checkin");
                }
            }
        );

    }, function(error) {
        console.log("Error - unable to find location - check browser security settings");
    },
        {timeout: 5000}
    );
}