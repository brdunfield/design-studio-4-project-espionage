$(document).ready(function() {
    // Authenticate with local storage token
    // If no match / doesn't exist, present login screen
    if (!localStorage["token"] || !localStorage["username"]) {
        window.location.href="loginTest.html";
    } else {
        $.post ( "http://ugrad.bitdegree.ca/~bethanydunfield/Espionage/Prototype/php/authenticate.php", {username: localStorage["username"], token: localStorage["token"]})
            .done (
                function(data) {
                    console.log(data);
                    if (!data.authentication) {
                        window.location.href="loginTest.html";
                    }
                }
        );     
    }

});

function hideErrorDiv() {
    $("#error").css({"display": "none"});
    
}

function showErrorDiv(data, action) {
    $("#error > .status").text("Status: " + ((data.success) ? "Success" : "Failure"));
    // TODO - more detailed messages for specific actions
    $("#error > .message").text((data.error) ? data.error : "");
    $("#error > .title").text((action) ? "Result of : " + action : "Notification");
    $("#error").css({"display": "block"});
    console.log(data);  
}