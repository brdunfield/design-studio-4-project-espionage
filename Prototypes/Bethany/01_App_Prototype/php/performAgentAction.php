<?php
    header('Access-Control-Allow-Origin: *');
    include('connect_DB.php');
    
    // Get Relevant data from POST
    $username = trim($_POST['username']);
    $actionType = trim($_POST['actionType']);
    $locLat = trim($_POST['Location_Lat']);
    $locLong = trim($_POST['Location_Long']);

    // Get Agent ID from DB
    $sql = "SELECT `Agent_ID`, `Bugs`, `Sweeps`, `Stakeouts`, `Location_Broadcast`, `Alive` FROM Espionage_Users WHERE Email='$username'";
    $queryResult = mysql_query($sql);
    if ($queryResult) {
        if(mysql_num_rows($queryResult) > 0) {
            while($rowData = mysql_fetch_assoc($queryResult)) {
                $id = $rowData['Agent_ID'];
                $bugs = $rowData['Bugs'];
                $sweeps = $rowData['Sweeps'];
                $stakeouts = $rowData['Stakeouts'];
                
                $exposed = $rowData['Location_Broadcast'];
                if ($exposed) {
                    $arr = array('error' => "You are exposed and cannot complete this action.");
                    header('Content-type: application/json');
                    echo json_encode($arr);
                    exit;
                }
                
                $alive = $rowData['Alive'];
                if (!$alive) {
                    $arr = array('error' => "You are neutralized and cannot perform any further actions.");
                    header('Content-type: application/json');
                    echo json_encode($arr);
                    exit;
                }
            }
        } else {
            $arr = array('error' => "Error getting Agent ID from Database");
            header('Content-type: application/json');
            echo json_encode($arr);
            exit;
        }
    }
    // Action limit - if the player does not have any of the action remaining, throw an error
    if (($actionType == 'sweep' && $sweeps <= 0) || ($actionType == 'bug' && $bugs <= 0) || ($actionType == 'stakeout' && $stakeouts <= 0)) {
        $arr = array('error' => "All of that action have been used up");
        header('Content-type: application/json');
        echo json_encode($arr);
        exit;
    }


    // If type is sweep, search DB for nearby bugs / stakeouts
    if ($actionType == 'sweep') {
        $sweeps --;
        // Update user table with reduced #
        $sql = "UPDATE `Espionage_Users` SET `Sweeps`='$sweeps' WHERE Agent_ID='$id'";
        $res = mysql_query($sql);
        if (!$res) {
            $arr = array('error' => "Error updating remaining Sweeps in DB");
            header('Content-type: application/json');
            echo json_encode($arr);
            exit;
        }
        // Add entry to feed
        $time = date("Y-m-d H:i:s");
        $feed = "INSERT INTO `Espionage_Feed` (`Agent_ID`, `Message`, `Timestamp`) VALUES (" . $id . ", 'You performed a Sweep.', \"$time\")";
        $feedRes = mysql_query($feed);
        if (!$feedRes) {
            $arr = array('error' => "Error inserting entry into feed");
            header('Content-type: application/json');
            echo json_encode($arr);
            exit;
        }
        
        // define range of influence
        $latLow = $locLat - 0.0001; // ~ 11m
        $latHigh = $locLat + 0.0001;
        $longLow = $locLong - 0.0001;
        $longHigh = $locLong + 0.0001;
        $sql = "SELECT * FROM Espionage_Actions WHERE `Type` IN ('bug', 'stakeout' ) AND Location_Lat BETWEEN " . $latLow . " AND " . $latHigh . " AND Location_Long BETWEEN " . $longLow . " AND " . $longHigh;
        $queryResult = mysql_query($sql);
        if ($queryResult) {
            $clearedBugs = 0;
            $clearedStakeouts = 0;
            if(mysql_num_rows($queryResult) > 0) {
                // For each found bug / stakeout, gain random info about the agent who left it
                // Then clear the bug / stakeout from the DB
                
                $newInfo = array(); // store newly found info to communicate success to client?
                $err = false;
                while($rowData = mysql_fetch_assoc($queryResult)) {
                    // 1. Get Agent ID from queryResult
                    $targetID = $rowData['Agent_ID'];
                    
                    // 2. Pick a random piece of info
                    // Define all col names as an array
                    $cols = array("First_Name", "Last_Name", "Gender", "Age");
                    $targetCols = array("Target_FName", "Target_LName", "Target_Gender", "Target_Age");
                    // Pick random index
                    $randKey = array_rand($cols);
                    // Store col names
                    $targetCol = $cols[$randKey];
                    $dossierCol = $targetCols[$randKey];
                    // N.B ONCe this is updated - will need to select * and determine what we have / don't have lower down
                    $agentQuery = "SELECT " . $targetCol . " FROM Espionage_Users WHERE Agent_ID='$targetID'";
                    $agentRes = mysql_query($agentQuery);
                    if ($agentRes) {
                        if(mysql_num_rows($agentRes) > 0) {
                            while ($agentRowData = mysql_fetch_assoc($agentRes)) {
                                // Add the found info to both the agent's DB dossier and the client update

                                $foundInfo = $agentRowData[$targetCol];
                                $infoEntry = array(
                                    'targetID' => $targetID,
                                    $dossierCol => $foundInfo
                                );
                                
                                // To add to dossier DB we first need to know if the player has any existing info on the target
                                // (i.e. - whether we're doing an INSERT or an UPDATE)
                                $dossierQuery = "SELECT * FROM Espionage_Dossier WHERE Agent_ID='$id' AND Target_ID='$targetID'";
                                $dossierRes = mysql_query($dossierQuery);
                                if ($dossierRes) {
                                    if(mysql_num_rows($dossierRes) > 0) {
                                        // Update
                                        // N.B. here is where we will determine what info to update
                                        
                                        // TODO - Intelligence "Tiers"
                                        
                                        $updateQuery = "UPDATE Espionage_Dossier SET `" . $dossierCol . "`='$foundInfo' WHERE Agent_ID='$id' AND Target_ID='$targetID'";
                                    } else {
                                        // Insert
                                        $updateQuery = "INSERT INTO `Espionage_Dossier` (`Agent_ID`, `Target_ID`, `" . $dossierCol . "`) VALUES (\"$id\", \"$targetID\", \"$foundInfo\")";
                                    }
                                    $updateRes = mysql_query($updateQuery);
                                    if (!$updateRes) {
                                        // Failure updating the DB
                                        $err = true;
                                        $arr = array('error' => "Could not Update Dossier - Agent Id:" . $id . ", TargetID:" . $targetID);
                                        header('Content-type: application/json');
                                        echo json_encode($arr);
                                        exit;
                                    }
                                    // Add entry to feed
                                    $time = date("Y-m-d H:i:s");
                                    $feed = "INSERT INTO `Espionage_Feed` (`Agent_ID`, `Message`, `Timestamp`) VALUES 
                                        (" . $id . ", 'You learned a piece of information about Agent #" . $targetID . " while performing a sweep.', \"$time\")";
                                    $feedRes = mysql_query($feed);
                                    if (!$feedRes) {
                                        $arr = array('error' => "Error inserting entry into feed");
                                        header('Content-type: application/json');
                                        echo json_encode($arr);
                                        exit;
                                    }
                                } else {
                                    // Failure querying dossier table
                                    $arr = array('error' => "Could not query Dossier Table");
                                    header('Content-type: application/json');
                                    echo json_encode($arr);
                                    exit;
                                }
                                
                            }
                        } else {
                            // something went wrong - we should always get 1 result
                            $arr = array('error' => "No results returned from User table - should not occur");
                            header('Content-type: application/json');
                            echo json_encode($arr);
                            exit;
                        }
                    } else {
                        $arr = array('error' => "Could not query User Table");
                        header('Content-type: application/json');
                        echo json_encode($arr);
                        exit;
                    }
                    // add new info to client update
                    array_push($newInfo, $infoEntry);
                    if ($rowData['Type'] == 'bug') {
                        $clearedBugs ++;
                    } else {
                        $clearedStakeouts ++;
                    }
                    
                    // Delete the bug / stakeout from the DB
                    $delquery = "DELETE FROM `Espionage_Actions` WHERE ID=" . $rowData['ID'];
                    $delResult = mysql_query($delquery);
                    if (!$delResult) {
                        $arr = array('error' => "Error deleting Action from DataBase");
                        header('Content-type: application/json');
                        echo json_encode($arr);
                        exit;
                    }
                }
                // Send client update
                if (!$err) {
                    $arr = array('dossierUpdates' => $newInfo, 'success' => true, 'type' => $actionType, 'error' => "Cleared " . $clearedBugs . " bugs and " . $clearedStakeouts . " stakeouts! Check your dossier for new information!");
                } else {
                    $arr = array('error' => "Failure updating the DB with new info");
                }
                header('Content-type: application/json');
                echo json_encode($arr);
            } else {
                // Return an alert to the user that the sweep failed (didn't find anything)
                $arr = array('dossierUpdates' => array(), 'success' => false, 'type' => $actionType, 'error' => "No bugs or stakeouts found nearby");
                header('Content-type: application/json');
                echo json_encode($arr);
                exit;
            }
        } else {
            $arr = array('error' => "Could not query Actions Table");
            header('Content-type: application/json');
            echo json_encode($arr);
            exit;
        }
        
        
    } else {    // If type is Bug or Stakeout - insert into DB
        if ($actionType == 'bug') {
            $bugs --;
            $sql = "UPDATE `Espionage_Users` SET `Bugs`='$bugs' WHERE Agent_ID='$id'";
            $message = "You placed a bug.";
        } else {
            $stakeouts --;
            $sql = "UPDATE `Espionage_Users` SET `Stakeouts`='$stakeouts' WHERE Agent_ID='$id'";
            $message = "You began a stakeout.";
        }
        $sweeps --;
        // Update user table with reduced #
        $res = mysql_query($sql);
        if (!$res) {
            $arr = array('error' => "Error updating remaining Action count in DB");
            header('Content-type: application/json');
            echo json_encode($arr);
            exit;
        }
        
        // Add entry to feed
        $time = date("Y-m-d H:i:s");
        $feed = "INSERT INTO `Espionage_Feed` (`Agent_ID`, `Message`, `Timestamp`) VALUES (" . $id . ", \"$message\", \"$time\")";
        $feedRes = mysql_query($feed);
        if (!$feedRes) {
            $arr = array('error' => "Error inserting entry into feed");
            header('Content-type: application/json');
            echo json_encode($arr);
            exit;
        }
        
        // Insert into DB
        
        $sql = "INSERT INTO Espionage_Actions (`type`, `Agent_ID`, `Location_Lat`, `Location_Long`) VALUES (\"$actionType\", \"$id\", " . $locLat . ", " . $locLong .")";
        $queryRes = mysql_query($sql);
        if (!$queryRes) {
            $arr = array('error' => "Could not insert new Action into Database");
            header('Content-type: application/json');
            echo json_encode($arr);
            exit;
        } else {
            // Send success confirmation to user
            $arr = array('type' => $actionType, 'success' => true);
            header('Content-type: application/json');
            echo json_encode($arr);
            exit;
        }
    }

?>
