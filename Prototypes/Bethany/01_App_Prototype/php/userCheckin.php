<?php
    header('Access-Control-Allow-Origin: *');
    //header('Content-type: application/json');
    include('connect_DB.php');
    
    // Get Relevant data from POST
    $username = trim($_POST['username']);
    $locLat = trim($_POST['Location_Lat']);
    $locLong = trim($_POST['Location_Long']);

    $exposedMSG = "";

    // Get Agent ID from DB
    $sql = "SELECT * FROM Espionage_Users WHERE Email='$username'";
    $queryResult = mysql_query($sql);
    if ($queryResult) {
        if(mysql_num_rows($queryResult) > 0) {
            while($rowData = mysql_fetch_assoc($queryResult)) {
                $id = $rowData['Agent_ID'];
                
                // Pick a random piece of info - in case we are near a bug or stakout
                // Define all col names as an array
                $cols = array("First_Name", "Last_Name", "Gender", "Age");
                $targetCols = array("Target_FName", "Target_LName", "Target_Gender", "Target_Age");
                // Pick random index
                $randKey = array_rand($cols);
                // Store col names
                $targetCol = $cols[$randKey];
                $dossierCol = $targetCols[$randKey];
                $foundInfo = $rowData[$targetCol];
                
                $exposed = $rowData['Location_Broadcast'];
                $exposedTime = $rowData['ExposedTime'];
                
                $alive = $rowData['Alive'];
                if (!$alive) {
                    // no need to continue
                    exit;
                }
            }
        } else {
            $arr = array('error' => "Error getting Agent ID from Database");
            header('Content-type: application/json');
            echo json_encode($arr);
            exit;
        }
    }
    $time = date("Y-m-d H:i:s");
    // First, if exposed, check if still exposed
    if ($exposed == 1) {
        $start = new DateTime($exposedTime);
        $interval = $start->diff(new DateTime($time));
        if (intval($interval->format("%i")) >= 5) {
            // Agent is no longer exposed - change DB and notify user as part of checkin
            // Update position in DB in two places - general user table and insert new entry into checkin table
            $updateSql = "UPDATE Espionage_Users SET `Location_Lat`='$locLat', `Location_Long`='$locLong', `Location_Broadcast`=0 WHERE Agent_ID='$id'";
            $updateRes = mysql_query($updateSql);
            
            $exposedMSG = "You are no longer exposed - 5 minutes has past";
            // Add Feed Entry
            
            // Add entry to feed
            $time = date("Y-m-d H:i:s");
            $feed = "INSERT INTO `Espionage_Feed` (`Agent_ID`, `Message`, `Timestamp`) VALUES (" . $id . ", 'You are no longer exposed.', \"$time\")";
            $feedRes = mysql_query($feed);
            if (!$feedRes) {
                $arr = array('error' => "Error inserting entry into feed");
                header('Content-type: application/json');
                echo json_encode($arr);
                exit;
            }
        }
    } else {
   
        // Update position in DB in two places - general user table and insert new entry into checkin table
        $updateSql = "UPDATE Espionage_Users SET `Location_Lat`='$locLat', `Location_Long`='$locLong' WHERE Agent_ID='$id'";
        $updateRes = mysql_query($updateSql);
    }
    $checkinSQL = "INSERT INTO `Espionage_Checkins` (`Agent_ID`, `Location_Lat`, `Location_Long`, `Time`) VALUES (\"$id\", \"$locLat\", \"$locLong\", \"$time\")";
    $checkinRes = mysql_query($checkinSQL);
    if (!$updateRes || !$checkinRes) {
        //DB error
        $arr = array('error' => "Database Error when checking in");
        header('Content-type: application/json');
        echo json_encode($arr);
        exit;
    }

    // check if within a Bug / Stakeout / etc and update the appropriate player's dossier
    // TODO - limit this check to once per day - i.e. the same bug cannot unlock more than once per day
    $latLow = $locLat - 0.0001; // ~ 11m
    $latHigh = $locLat + 0.0001;
    $longLow = $locLong - 0.0001;
    $longHigh = $locLong + 0.0001;
    $sql = "SELECT * FROM Espionage_Actions WHERE `Type` IN ('stakeout', 'bug') AND `Agent_ID` <> " . $id . " AND Location_Lat BETWEEN " . $latLow . " AND " . $latHigh . " AND Location_Long BETWEEN " . $longLow . " AND " . $longHigh;
    $res = mysql_query($sql);
    if ($res) {
        if(mysql_num_rows($res) > 0) {
            while($action = mysql_fetch_assoc($res)) {
                $agentID = $action['Agent_ID'];
                $type = $action['Type'];
                
                // Add a feed update to the user who placed the bug / stakeout
                $feed = "INSERT INTO `Espionage_Feed` (`Agent_ID`, `Message`, `Timestamp`) VALUES 
                    (" . $agentID . ", 'You learned a piece of information about Agent #" . $id . " from your " . $type . ".', \"$time\")";
                $feedRes = mysql_query($feed);
                if (!$feedRes) {
                    $arr = array('error' => "Error inserting entry into feed");
                    header('Content-type: application/json');
                    echo json_encode($arr);
                    exit;
                }
                
                
                // Determine whether to use UPDATE or INSERT (i.e. whether the agent already has information on the player)
                $dossierQuery = "SELECT * FROM Espionage_Dossier WHERE Agent_ID='$agentID' AND Target_ID='$id'";
                $dossierRes = mysql_query($dossierQuery);
                if ($dossierRes) {
                    if(mysql_num_rows($dossierRes) > 0) {
                        // Update
                        $updateQuery = "UPDATE Espionage_Dossier SET `" . $dossierCol . "`='$foundInfo' WHERE Agent_ID='$agentID' AND Target_ID='$id'";
                    } else {
                        // Insert
                        $updateQuery = "INSERT INTO `Espionage_Dossier` (`Agent_ID`, `Target_ID`, `" . $dossierCol . "`) VALUES (\"$agentID\", \"$id\", \"$foundInfo\")";
                    }

                    $updateRes = mysql_query($updateQuery);
                    if ($updateRes) {
                        // Add entry to feed
                        $time = date("Y-m-d H:i:s");
                        $message = "You were caught in another agent's " . $type . ".";
                        $feed = "INSERT INTO `Espionage_Feed` (`Agent_ID`, `Message`, `Timestamp`) VALUES (" . $id . ", \"$message\", \"$time\")";
                        $feedRes = mysql_query($feed);
                        if (!$feedRes) {
                            $arr = array('error' => "Error inserting entry into feed");
                            header('Content-type: application/json');
                            echo json_encode($arr);
                            exit;
                        } 
                        
                        // Send a notification they were caught.
                        $arr = array('success' => true, 'error' => "You were caught in another agent's Bug or Stakeout.");
                        header('Content-type: application/json');
                        echo json_encode($arr);
                        exit;
                    } else {
                        // Failure updating the DB
                        $arr = array('error' => "You were caught in a Bug / Stakeout. However, there was a Database failure giving them dossier information about you");
                        header('Content-type: application/json');
                        echo json_encode($arr);
                        exit;
                    }
                } else {
                    // Failure querying dossier table
                    $arr = array('error' => "You were caught in a bug / Stakeout. However, Could not query Dossier Table in Database");
                    header('Content-type: application/json');
                    echo json_encode($arr);
                    exit;
                }
            }
        } // else - no bugs / stakeouts found - no need to do anything
    } else {
        $arr = array('error' => "Unable to select bugs or stakeouts from Action table during Checkin");
        header('Content-type: application/json');
        echo json_encode($arr);
        exit;
    }

    // Cancel any user placed stakeouts if distance has grown too far
    $stakeoutQuery = "SELECT * FROM `Espionage_Actions` WHERE Agent_ID=" . $id . " AND `Type`='stakeout' AND ((Location_Lat NOT BETWEEN " . $latLow . " AND " . $latHigh . ") OR (Location_Long NOT BETWEEN " . $longLow . " AND " . $longHigh . "))";
//die($stakeoutQuery);
    $result = mysql_query($stakeoutQuery);
    if ($result) {
        if(mysql_num_rows($result) > 0) {
            while($stakeout = mysql_fetch_assoc($result)) {
                // Delete from DB and notify user
                $delquery = "DELETE FROM `Espionage_Actions` WHERE ID=" . $stakeout['ID'];
                $delResult = mysql_query($delquery);
                if ($delResult) {
                    // Add entry to feed
                    $time = date("Y-m-d H:i:s");
                    $feed = "INSERT INTO `Espionage_Feed` (`Agent_ID`, `Message`, `Timestamp`) VALUES (" . $id . ", 'Your stakeout ended because you left the area.', \"$time\")";
                    $feedRes = mysql_query($feed);
                    if (!$feedRes) {
                        $arr = array('error' => "Error inserting entry into feed");
                        header('Content-type: application/json');
                        echo json_encode($arr);
                        exit;
                    }
                    
                    $arr = array('success' => true, 'error' => "Your stakeout has ended because you moved too far away", 'type' => $stakeout['Type']);
                    header('Content-type: application/json');
                    echo json_encode($arr);
                    exit;
                } else {
                    $arr = array('error' => "Error deleting Stakeout from Database during Checkin due to distance");
                    header('Content-type: application/json');
                    echo json_encode($arr);
                    exit;
                }
            }
        }// else - no stakeouts found - no need to do anything
    } else {
        $arr = array('error' => "Unable to select stakeouts from Action table during Checkin");
        header('Content-type: application/json');
        echo json_encode($arr);
        exit;
    }

    // DEBUG: Send back success confirmation
    $arr = array('success' => true, 'error' => $exposedMSG);
    header('Content-type: application/json');
    echo json_encode($arr);
    exit;

?>
