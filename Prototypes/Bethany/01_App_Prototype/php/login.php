<?php
    header('Access-Control-Allow-Origin: *');    
    include('connect_DB.php');
    // Get Login info from Post request
    $username = trim($_POST['username']);
    $password = md5(trim($_POST['password']));

    // Lookup credentials in DB
    $sql = "SELECT Agent_ID, Email, Password FROM Espionage_Users WHERE Email ='$username'";
    $queryResult = mysql_query($sql);

    if ($queryResult) {
        if(mysql_num_rows($queryResult) > 0) {
            while($rowData = mysql_fetch_assoc($queryResult)) {
                // TODO - hash plaintext passwords
                if ($password == $rowData['Password']) {
                    $userID = $rowData['Agent_ID'];
                    startSession($userID);
                } else {
                    header('Content-type: application/json');
                    $arr = array('error' => "Password does not match");
                    echo json_encode($arr);
                }
            }
        } else {
            header('Content-type: application/json');
            $arr = array('error' => "No user exists with that email");
            echo json_encode($arr);
        }
    }


    function startSession($userID) {
        // Store random token in cookie on client and in DB on server
        $randomToken = hash('sha256',uniqid(mt_rand(), true).uniqid(mt_rand(), true));
        $query = "UPDATE IGNORE Espionage_Users SET token='$randomToken' WHERE Agent_ID='$userID'";
        // Update DB
        $result = mysql_query($query);
        if (!$result) {
            die('Invalid query: ' . mysql_error());
        }
        // Send token to client for storage within a cookie - and confirmation the login worked
        $arr = array('token' => $randomToken);
        header('Content-type: application/json');
        echo json_encode($arr);
        
        
        // Start session on server
        session_start();
        $_SESSION['Agent_ID'] = $userID;
    }

?>
