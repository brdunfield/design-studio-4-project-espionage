<?php
    header('Access-Control-Allow-Origin: *');
    include('connect_DB.php');
    
    // Get username from post
    $username = trim($_POST['username']);

    // Get Agent ID from DB
    $sql = "SELECT Agent_ID FROM Espionage_Users WHERE Email='$username'";
    $queryResult = mysql_query($sql);
    if ($queryResult) {
        if(mysql_num_rows($queryResult) > 0) {
            while($rowData = mysql_fetch_assoc($queryResult)) {
                $id = $rowData['Agent_ID'];
            }
        } else {
            $arr = array('error' => "Error getting Agent ID from Database");
            header('Content-type: application/json');
            echo json_encode($arr);
            exit;
        }
    }
    // Get feed entries pertaining to this agent
    $sql = "SELECT `Message`, `Timestamp` FROM Espionage_Feed WHERE Agent_ID=" . $id . " ORDER BY `Timestamp` DESC";
    $queryResult = mysql_query($sql);
    if ($queryResult) {
        if(mysql_num_rows($queryResult) > 0) {
            $feedEntries = array();
            while($rowData = mysql_fetch_assoc($queryResult)) {
                $feedEntry = array(
                    'message' => $rowData['Message'],
                    'time' => $rowData['Timestamp']
                );
                array_push($feedEntries, $feedEntry);
            }
            // output to client
            $arr = array('feedEntries' => $feedEntries);
            header('Content-type: application/json');
            echo json_encode($arr);
            exit;
        } else {
            $arr = array('success' => true, 'error' => "No Messages");
            header('Content-type: application/json');
            echo json_encode($arr);
            exit;
        }
    } else {
        $arr = array('success' => false, 'error' => "Error selecting Feed Entries from Database");
        header('Content-type: application/json');
        echo json_encode($arr);
        exit;
    }

?>
