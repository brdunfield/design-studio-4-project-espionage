<?php
    header('Access-Control-Allow-Origin: *');
    include('connect_DB.php');
    
    // Get Relevant data from POST
    $username = trim($_POST['username']);
    $locLat = trim($_POST['Location_Lat']);
    $locLong = trim($_POST['Location_Long']);

    // Get Agent ID from DB
    $sql = "SELECT `Agent_ID`, `Kills`, `Location_Broadcast`, `Alive` FROM Espionage_Users WHERE Email='$username'";
    $queryResult = mysql_query($sql);
    if ($queryResult) {
        if(mysql_num_rows($queryResult) > 0) {
            while($rowData = mysql_fetch_assoc($queryResult)) {
                $id = $rowData['Agent_ID'];
                $kills = $rowData['Kills'];
                
                $exposed = $rowData['Location_Broadcast'];
                if ($exposed) {
                    $arr = array('error' => "You are exposed and cannot complete this action.");
                    header('Content-type: application/json');
                    echo json_encode($arr);
                    exit;
                }
                $alive = $rowData['Alive'];
                if (!$alive) {
                    $arr = array('error' => "You are neutralized and cannot perform any further actions.");
                    header('Content-type: application/json');
                    echo json_encode($arr);
                    exit;
                }
            }
        } else {
            $arr = array('error' => "Error getting Agent ID from Database");
            header('Content-type: application/json');
            echo json_encode($arr);
            exit;
        }
    }

    // Find any agents nearby that are still alive and are not the player
    $latLow = $locLat - 0.0002; // ~ 22m
    $latHigh = $locLat + 0.0002;
    $longLow = $locLong - 0.0002;
    $longHigh = $locLong + 0.0002;
    $agentQuery = "SELECT * FROM Espionage_Users WHERE `Agent_ID`<>'$id' AND `Alive`=1 AND `Location_Lat` BETWEEN " . $latLow . " AND " . $latHigh . " AND Location_Long BETWEEN " . $longLow . " AND " . $longHigh;
    $agentRes = mysql_query($agentQuery);
    if ($agentRes) {
        if(mysql_num_rows($agentRes) > 0) {
            while($agentRowData = mysql_fetch_assoc($agentRes)) {
                // TODO - do this more intelligently - for now just kill first agent we see
                $targetID = $agentRowData['Agent_ID'];
                $neutralizeQuery = "UPDATE Espionage_Users SET `Alive`=0 WHERE Agent_ID='$targetID'";
                $neutRes = mysql_query($neutralizeQuery);
                if ($neutRes) {
                    // increase the player's kills by one
                    $newKills = $kills + 1;
                    $successQuery = "UPDATE Espionage_Users SET `Kills`='$newKills' WHERE Agent_ID='$id'";
                    $successRes = mysql_query($successQuery);
                    if (!$successRes) {
                        // Throw DB error
                        $arr = array('error' => "Error updating kill count in Database");
                        header('Content-type: application/json');
                        echo json_encode($arr);
                        exit;
                    }
                    
                    
                    // Add entry to feed
                    $time = date("Y-m-d H:i:s");
                    $feed = "INSERT INTO `Espionage_Feed` (`Agent_ID`, `Message`, `Timestamp`) VALUES (" . $id . ", 'You have successfully neutralized Agent #" . $targetID . ".', \"$time\"), (" . $targetID . ", 'You have been neutralized by Agent #" . $id . ".', \"$time\")";
                    $feedRes = mysql_query($feed);
                    if (!$feedRes) {
                        $arr = array('error' => "Error inserting entry into feed");
                        header('Content-type: application/json');
                        echo json_encode($arr);
                        exit;
                    }
                    // send success message
                    $arr = array('success' => true);
                    header('Content-type: application/json');
                    echo json_encode($arr);
                    exit;
                    
                } else {
                    $arr = array('error' => "Error neutralizing player in Database");
                    header('Content-type: application/json');
                    echo json_encode($arr);
                    exit;
                }
                
                
                /*
                // If we encounter an exposed agent here - just perform the neutralization and exit
                $exposed = $rowData['Location_Broadcast'];
                if ($exposed == 1) {
                    $targetID = $rowData['Agent_ID'];
                    $neutralizeQuery = "UPDATE Espionage_Users SET `Alive`=0 WHERE Agent_ID='$targetID'";
                }
                */
            }
        } else {
            // No agents nearby - expose the player
            $time = date("Y-m-d H:i:s");
            $exposeQuery = "UPDATE Espionage_Users SET `Location_Broadcast`=1, `ExposedTime`=\"$time\" WHERE Agent_ID='$id'";
            $exposeRes = mysql_query($exposeQuery);
            if($exposeRes) {
                // Add entry to feed
                $feed = "INSERT INTO `Espionage_Feed` (`Agent_ID`, `Message`, `Timestamp`) VALUES (" . $id . ", 'Your Neutralization attempt failed. You are now exposed.', \"$time\")";
                $feedRes = mysql_query($feed);
                if (!$feedRes) {
                    $arr = array('error' => "Error inserting entry into feed");
                    header('Content-type: application/json');
                    echo json_encode($arr);
                    exit;
                }
                
                // let the player know they've been exposed
                $arr = array('success' => false, 'error' => "No players found nearby - Neutralize failed, you are now exposed");
                header('Content-type: application/json');
                echo json_encode($arr);
                exit;
            } else {
                // Failure to update DB
                $arr = array('error' => "Error exposing player in Database");
                header('Content-type: application/json');
                echo json_encode($arr);
                exit;
            }
        }
    } else {
        // Error selecting agents from database
        $arr = array('error' => "Error selecting agents from database");
        header('Content-type: application/json');
        echo json_encode($arr);
        exit;
    }

?>
