<?php
    header('Access-Control-Allow-Origin: *');
    //header('Content-type: application/json');
    include('connect_DB.php');
    
    // Get Relevant data from POST
    $username = trim($_POST['username']);
    $actionType = trim($_POST['actionType']);
    $locLat = trim($_POST['Location_Lat']);
    $locLong = trim($_POST['Location_Long']);

    // Types:
    // x Dead Drop
    // x Dead Drop Pickup
    // ~ Hacking
    // ~ Rendezvous
    // - Supply Drop
    // X Safehouse

    // Get Agent ID from DB
    $sql = "SELECT `Agent_ID`, `Location_Broadcast`, `Bugs`, `Sweeps`, `Stakeouts`, `Location_Broadcast`, `Alive` FROM Espionage_Users WHERE Email='$username'";
    $queryResult = mysql_query($sql);
    if ($queryResult) {
        if(mysql_num_rows($queryResult) > 0) {
            while($rowData = mysql_fetch_assoc($queryResult)) {
                $id = $rowData['Agent_ID'];
                $exposed = $rowData['Location_Broadcast'];
                $bugs = $rowData['Bugs'];
                $sweeps = $rowData['Sweeps'];
                $stakeouts = $rowData['Stakeouts'];
                
                $exposed = $rowData['Location_Broadcast'];
                if ($exposed) {
                    $arr = array('error' => "You are exposed and cannot complete this action.");
                    header('Content-type: application/json');
                    echo json_encode($arr);
                    exit;
                }
                $alive = $rowData['Alive'];
                if (!$alive) {
                    $arr = array('error' => "You are neutralized and cannot perform any further actions.");
                    header('Content-type: application/json');
                    echo json_encode($arr);
                    exit;
                }
            }
        } else {
            $arr = array('error' => "Error getting Agent ID from Database");
            echo json_encode($arr);
            exit;
        }
    }

    // First determine if an active mission is within the acceptable radius
    $latLow = $locLat - 0.0001; // ~ 11m
    $latHigh = $locLat + 0.0001;
    $longLow = $locLong - 0.0001;
    $longHigh = $locLong + 0.0001;
    if ($actionType == 'deaddrop' || $actionType == 'deaddroppickup') {
        // Dead Drops / Pickups perform more like Agent Actions - I will store Dead Drops waiting for pickup
        // In the "Actions" table - but N.B. They need to be plotted on the map as well
        
        // Dead Drop - Add new action into table
        if ($actionType == 'deaddrop') {
            $sql = "INSERT INTO Espionage_Actions (`type`, `Agent_ID`, `Location_Lat`, `Location_Long`) VALUES (\"$actionType\", \"$id\", " . $locLat . ", " . $locLong .")";
            $result = mysql_query($sql);
            if (!$result) {
                $arr = array('error' => "Error inserting dead drop into database", 'success' => false);
                header('Content-type: application/json');
                echo json_encode($arr);
                exit;
            } else {
                // Add entry to feed
                $time = date("Y-m-d H:i:s");
                $feed = "INSERT INTO `Espionage_Feed` (`Agent_ID`, `Message`, `Timestamp`) VALUES (" . $id . ", 'You deployed a Dead Drop.', \"$time\")";
                $feedRes = mysql_query($feed);
                if (!$feedRes) {
                    $arr = array('error' => "Error inserting entry into feed");
                    header('Content-type: application/json');
                    echo json_encode($arr);
                    exit;
                }
                
                // send confirmation to user
                $arr = array('success' => true, 'type' => $actionType);
                header('Content-type: application/json');
                echo json_encode($arr);
                exit;
            }
        } else { // Dead Drop pickup - Copy data from leaver of the dead drop's dossier into the players dossier
            // First see if the dead drop is nearby and get the agent id who left it
            $dropQuery = "Select `Agent_ID`, `ID` FROM `Espionage_Actions` WHERE Location_Lat BETWEEN " . $latLow . " AND " . $latHigh . " AND Location_Long BETWEEN " . $longLow . " AND " . $longHigh . "
                AND `Type`='deaddrop'";
            $dropRes = mysql_query($dropQuery);
            if ($dropRes) {
                if (mysql_num_rows($dropRes) > 0) {
                    while($dropRow = mysql_fetch_assoc($dropRes)) {
                        $agentID = $dropRow['Agent_ID'];
                        // Now select all the rows from their dossier and for each row, add / update rows in the player's dossier
                        $getInfoQuery = "SELECT * FROM `Espionage_Dossier` WHERE `Agent_ID`='$agentID' AND `Target_ID` <>'$id'";
                        $getInfoRes = mysql_query($getInfoQuery);
                        if ($getInfoRes) {
                            if (mysql_num_rows($getInfoRes) > 0) {
                                while ($info = mysql_fetch_assoc($getInfoRes)) {
                                    $targetID = $info['Target_ID'];
                                    //$targetFName = $info['Target_FName']
                                    $dossierQuery = "SELECT * FROM Espionage_Dossier WHERE Agent_ID='$id' AND Target_ID='$targetID'";
                                    $dossierRes = mysql_query($dossierQuery);
                                    if ($dossierRes) {
                                        if(mysql_num_rows($dossierRes) > 0) {
                                            // Update
                                            // N.B. here is where we will determine whether to update or insert new data
                                            $updateQuery = "UPDATE `Espionage_Dossier` SET " . (($info['Target_FName'] == "") ? "" : "`Target_FName`='" . $info['Target_FName'] . "', ") . 
                                                (($info['Target_LName'] == "") ? "" : "`Target_LName`='" . $info['Target_LName'] . "', ") . 
                                                (($info['Target_Age'] == "") ? "" : "`Target_Age`=" . $info['Target_Age'] . ", ") . 
                                                (($info['Target_Gender'] == "") ? "" : "`Target_Gender`='" . $info['Target_Gender'] . "', ") . 
                                                "WHERE Agent_ID='$id' AND Target_ID='$targetID'";
                                            $updateQuery = str_replace(", W", " W", $updateQuery); // remove trailing commas
                                        } else {
                                            // Insert
                                            $updateQuery = "INSERT INTO `Espionage_Dossier` (`Agent_ID`, `Target_ID`, `Target_FName`, `Target_LName`, `Target_Age`, `Target_Gender`) 
                                                VALUES (\"$id\", \"$targetID\", " . $info['Target_FName'] . ", " . $info['Target_LName'] . ", " . $info['Target_Age'] . ", " . $info['Target_Gender'] .")";
                                        }
                                        //die($updateQuery);
                                        $updateRes = mysql_query($updateQuery);
                                        if (!$updateRes) {
                                            // Failure updating the DB
                                            $err = true;
                                            $arr = array('error' => "Could not Update Dossier - Agent Id:" . $id . ", TargetID:" . $targetID);
                                            header('Content-type: application/json');
                                            echo json_encode($arr);
                                            exit;
                                        }
                                    }
                                }
                            } else {
                                $arr = array('error' => "The dead drop is empty", 'success' => false);
                                header('Content-type: application/json');
                                echo json_encode($arr);
                                exit;
                            }
                        } else {
                            $arr = array('error' => "Error getting Dossier info from Database", 'success' => false);
                            header('Content-type: application/json');
                            echo json_encode($arr);
                            exit;
                        }
                        
                        // After we are finished updating the dossier - remove the deaddrop
                        $del = "DELETE FROM `Espionage_Actions` WHERE ID=" . $dropRow['ID'];
                        $delRes = mysql_query($del);
                        if (!$delRes) {
                            $arr = array('error' => "Error deleting Dead Drop from DB", 'success' => false);
                            header('Content-type: application/json');
                            echo json_encode($arr);
                            exit;
                        } else {
                            // All has executed correctly
                            // Add entry to feed
                            $time = date("Y-m-d H:i:s");
                            $feed = "INSERT INTO `Espionage_Feed` (`Agent_ID`, `Message`, `Timestamp`) VALUES (" . $id . ", 'You successfully picked up a dead drop.', \"$time\")";
                            $feedRes = mysql_query($feed);
                            if (!$feedRes) {
                                $arr = array('error' => "Error inserting entry into feed");
                                header('Content-type: application/json');
                                echo json_encode($arr);
                                exit;
                            }
                            // inform user
                            $arr = array('success' => true, 'error' => "Successfully picked up the dead drop! Check your dossier for new information!");
                            header('Content-type: application/json');
                            echo json_encode($arr);
                            exit;
                        }
                    }
                } else {
                    $arr = array('error' => "No Dead Drop found nearby", 'success' => false);
                    header('Content-type: application/json');
                    echo json_encode($arr);
                    exit;
                }
            } else {
                $arr = array('error' => "Error selecting Dead Drop from Database", 'success' => false);
                header('Content-type: application/json');
                echo json_encode($arr);
                exit;
            }
        }
        
    } else {
        // TODO - select based on dates
        $missionQuery = "SELECT `Mission_ID`, `Type` FROM Espionage_Missions WHERE Location_Lat BETWEEN " . $latLow . " AND " . $latHigh . " AND Location_Long BETWEEN " . $longLow . " AND " . $longHigh . " 
            AND `Mission_ID` NOT IN ( SELECT `Mission_ID` FROM Espionage_MissionCompletion WHERE Agent_ID='$id')"; // Don't allow players to complete the same mission twice
        $mRes = mysql_query($missionQuery);
        $missionID = null;
        if ($mRes) {
            if(mysql_num_rows($mRes) > 0) {
                while($rowData = mysql_fetch_assoc($mRes)) {
                    // Check if its the right type of mission
                    
                    if ($actionType == $rowData['Type']) {
                        // Check if the player has already completed this mission (in case they remember where it was and try again)
                        $missionID = $rowData['Mission_ID'];
                    } else {
                        $arr = array('error' => "The nearby mission is the wrong type for that action", 'success' => false);
                        header('Content-type: application/json');
                        echo json_encode($arr);
                        exit;
                    }
                }
            } else {
                // No valid missions found - return the user an error message
                $arr = array('error' => "No Missions found nearby", 'success' => false);
                header('Content-type: application/json');
                echo json_encode($arr);
                exit;
            }
        } else {
            $arr = array('error' => "Error selecting Missions from Database", 'success' => false);
            header('Content-type: application/json');
            echo json_encode($arr);
            exit;
        }
        
        
        // Next - perform mission specific actions
        $missionData = array();
        
        /*
            SAFEHOUSE
            - Removes "Exposed" Status
        */
        if ($actionType == 'safehouse') {
            if ($exposed == 1) {
                // Change agent status to unexposed ... && checkin??
                $updateQuery = "UPDATE Espionage_Users SET `Location_Broadcast`=0 WHERE Agent_ID='$id'";
                $updateRes = mysql_query($updateQuery);
                if (!$updateRes) {
                    $arr = array('error' => "Could not remove Exposed Status", 'success' => false);
                    header('Content-type: application/json');
                    echo json_encode($arr);
                    exit;
                } // no else - goes to bottom code to send response to client
            } else {
                // Agent is not exposed - cannot complete action
                $arr = array('success' => false, 'type' => $actionType, 'error' => "Not Exposed - can't complete mission");
                header('Content-type: application/json');
                echo json_encode($arr);
                exit;

            }
        } else if ($actionType == 'hacking' || $actionType == 'rendezvous') {
            /*
                - Adds info on random agents to the player's dossier
                
                -Hacking
                TODO - change of learning location of a bug
                
                - Rendezvous
                increases available sweeps and stakeouts in the players inventory
            */
            
            // Select random number from 1-3 - this is the number of agents we will learn about
            $numAgents = rand(1, 3);
            $agentQuery = "SELECT * FROM `Espionage_Users` WHERE `Alive`=1 AND `Agent_ID` <>'$id' ORDER BY RAND() LIMIT " . $numAgents;
            //die($agentQuery);
            $agentRes = mysql_query($agentQuery);
            if ($agentRes) {
                if (mysql_num_rows($agentRes) > 0) {
                    while($agentRowData = mysql_fetch_assoc($agentRes)) {
                        // For each returned row, add a piece of information to the player's dossier
                        
                        // Pick a random piece of data about the agent
                        
                        // Define all col names as an array
                        $cols = array("First_Name", "Last_Name", "Gender", "Age");
                        $targetCols = array("Target_FName", "Target_LName", "Target_Gender", "Target_Age");
                        // Pick random index
                        $randKey = array_rand($cols);
                        // Store col names
                        $targetCol = $cols[$randKey];
                        $dossierCol = $targetCols[$randKey];
                        // TODO - send new info to user so they know what has changed - use $missionData variable
                        
                        // first need to determine if update or insert
                        
                        $targetID = $agentRowData['Agent_ID'];
                        $foundInfo = $agentRowData[$targetCol];
                        $dossierQuery = "SELECT * FROM Espionage_Dossier WHERE Agent_ID='$id' AND Target_ID='$targetID'";
                        $dossierRes = mysql_query($dossierQuery);
                        if ($dossierRes) {
                            if(mysql_num_rows($dossierRes) > 0) {
                                // Update
                                $dossierQuery = "UPDATE Espionage_Dossier SET `" . $dossierCol . "`='$foundInfo' WHERE Agent_ID='$id' AND Target_ID='$targetID'";
                            } else {
                                // Insert
                                $dossierQuery = "INSERT INTO `Espionage_Dossier` (`Agent_ID`, `Target_ID`, `" . $dossierCol . "`) VALUES (\"$id\", " . $agentRowData['Agent_ID'] . ", \"$foundInfo\")";
                            }
                            $dossierRes = mysql_query($dossierQuery);
                            if (!$dossierRes) {
                                $arr = array('error' => "Could not create new Dossier entry", 'success' => false);
                                header('Content-type: application/json');
                                echo json_encode($arr);
                                exit;
                            } // no else - goes to bottom code piece to return to client
                        } else {
                        $arr = array('error' => "Could not get info from Dossier", 'success' => false);
                        header('Content-type: application/json');
                        echo json_encode($arr);
                        exit;
                        }

                    }
                } else {
                    $arr = array('error' => "Could not find any players to get information about", 'success' => false);
                    header('Content-type: application/json');
                    echo json_encode($arr);
                    exit;
                }
            } else {
                $arr = array('error' => "Could not get agent info from Database", 'success' => false);
                header('Content-type: application/json');
                echo json_encode($arr);
                exit;
            }
            
            if ($actionType == 'hacking') {
                // TODO chance to find a bug
                $rand = rand(0, 10);
                if ($rand > 5) { // 40% chance to find one
                    // Do SQL query here
                    // $add found bug to $missionData variable
                }
            } else {
                // increase sweeps and stakeouts
                $sweeps += 2;
                $stakeouts += 2;
                $sql = "UPDATE `Espionage_Users` SET `Sweeps`='$sweeps', `Stakeouts`='$stakeouts' WHERE Agent_ID='$id'";
                $res = mysql_query($sql);
                if (!$res) {
                    $arr = array('error' => "Error giving user more Sweeps and Stakeouts from Rendezvous");
                    header('Content-type: application/json');
                    echo json_encode($arr);
                    exit;
                }
            }
        } else if ($actionType == 'supplydrop') {
            // Supplydrop increases # of bugs available to user
            $bugs += 3;
            $sql = "UPDATE `Espionage_Users` SET `Bugs`='$bugs' WHERE Agent_ID='$id'";
            $res = mysql_query($sql);
            if (!$res) {
                $arr = array('error' => "Error giving user more Bugs from Supply Drop");
                header('Content-type: application/json');
                echo json_encode($arr);
                exit;
            }
        }
        
        // Finally - update DB with completed mission status and inform player of success
        $time = date("Y-m-d H:i:s");
        // Complete this mission (INSERT to DB)
        $missionQuery = "INSERT INTO `Espionage_MissionCompletion` (`Agent_ID`, `Mission_ID`, `Time_Completed`) VALUES (\"$id\", \"$missionID\", \"$time\")";
        $mRes = mysql_query($missionQuery);
        if ($mRes) {
            // Add entry to feed
            $time = date("Y-m-d H:i:s");
            $message = "You successfully completed a " . $actionType . " Mission.";
            $feed = "INSERT INTO `Espionage_Feed` (`Agent_ID`, `Message`, `Timestamp`) VALUES (" . $id . ", \"$message \", \"$time\")";
            $feedRes = mysql_query($feed);
            if (!$feedRes) {
                $arr = array('error' => "Error inserting entry into feed");
                header('Content-type: application/json');
                echo json_encode($arr);
                exit;
            }
            
            // send confirmation to user
            $arr = array('success' => true, 'type' => $actionType, 'missionData' => $missionData);
            header('Content-type: application/json');
            echo json_encode($arr);
            exit;
        } else {
            // send failure to user
            $arr = array('error' => "Could not Complete Mission in Database", 'success' => false);
            header('Content-type: application/json');
            echo json_encode($arr);
            exit;
        }
        
    }

?>
