function initialize() {
    // Init Google Map centered on Ottawa
    var mapOptions = {
      center: { lat: 45.4, lng: -75.7},
      zoom: 13
    };
    var map = new google.maps.Map(document.getElementById('map-canvas'),
        mapOptions);

    // Plot position of device on the map
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var latLong = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            /*
            var infowindow = new google.maps.InfoWindow({
                map: map,
                position: latLong,
                content: 'Location found using HTML5.'
            });
            */
            var marker = new google.maps.Marker({
                position: latLong,
                map: map,
                title:"Your Device"
            });                    
        }, function(error) {
            console.log("Nope, didn't work.");
        }, {timeout: 5000}
        );
    } else {
        console.log("not supported");// Not supported
    }
}

// Call initialize function
google.maps.event.addDomListener(window, 'load', initialize);